import axios from 'axios';
const baseURL = process.env.NODE_ENV === 'development' ? '/api' : '';
const baseURL2 = process.env.NODE_ENV === 'development' ? ' /noapk/notice' : 'http://10.3.208.107:8580/kingyea-notice';


export const getTableApi = (params) => {
  return axios.get(baseURL+'/console/data/resources/stat/getTravelAndWBDate.do',{
    params: {
      tableNames: params
    }
  }).then((res) => res);
}


// 获取通知通报
export const getNoticeList = (params) => {
  return axios.get(baseURL2+'/n/p',{
    params : params
  }).then((res) => res);
}

export const getNoticeDetail = (params) => {
  return axios.get(baseURL2+'/n/g',{
    params : params
  }).then((res) => res);
}


// 获取用户信息
export const getUserInfo = (params) => {
  return axios.get('/platform/common/remote?service=sys.sysUserService.getById',{
    params: params
  }).then((res) => res);
}


export const loginOut = (params) => {
  return axios.get('/platform/system/tokenLogout',{
    params: params
  }).then((res) => res);
}