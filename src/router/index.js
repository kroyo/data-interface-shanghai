import Vue from 'vue'
import Router from 'vue-router'
import indexPage from '@/views/index'
import homePage from '@/views/homePage'
import passengerPage from '@/views/passengerPage'
import detail from '@/views/detail'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'index',
      component: indexPage
    },
    {
      path: '/index',
      name: 'index',
      component: indexPage
    },
    {
      path: '/home',
      name: 'home',
      component: homePage
    },
    {
      path: '/passenger',
      name: 'passenger',
      component: passengerPage
    },
    {
      path: '/detail/:id',
      name: 'detail',
      component: detail
    }
  ]
})
