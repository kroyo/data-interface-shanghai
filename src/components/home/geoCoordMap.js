const geoCoordMap = {
    '上海': [121.4648,31.2900],
    '上海南': [121.3648,30.9000],
    '上海虹桥': [121.1250,31.0690],
    '杭州': [119.5300,29.8773],
    '杭州东': [119.8300,30.1000],
    '杭州南': [119.9313,29.6773],
    '南京': [118.8062,32.0208],
    '南京南': [118.8062,31.7208],
    '徐州': [117.5208,34.3268],
    '徐州东': [118.2008,34.2000],
    '合肥': [117.29,32.1000],
    '合肥南': [117.70,31.7600],
    '合肥西': [116.80,31.9000],
    '蚌埠': [116.29,32.8581],
    '蚌埠南': [116.59,32.5581],

    '扬州': [119.4653,32.8162],
    '苏州': [120.6519,31.3989],
    '嘉兴': [120.9155,30.6354],
    '宁波': [121.5967,29.6466],
    '无锡': [120.3442,31.5527],
};

let SHData = [
    [{name:'上海'},{name:'南京南',value:95000}],
    [{name:'上海'},{name:'杭州东',value:90099}],
    [{name:'上海'},{name:'上海南',value:80333}],
    // [{name:'上海'},{name:'上海虹桥',value:70000}],
    [{name:'上海'},{name:'杭州',value:50220}],
    // [{name:'上海'},{name:'杭州南',value:4000}],
    [{name:'上海'},{name:'南京',value:30440}],
    [{name:'上海'},{name:'徐州',value:20550}],
    [{name:'上海'},{name:'徐州东',value:32060}],
    [{name:'上海'},{name:'合肥',value:40450}],
    [{name:'上海'},{name:'合肥西',value:30670}],
    // [{name:'上海'},{name:'合肥南',value:20}],
    [{name:'上海'},{name:'蚌埠',value:30999}],
    // [{name:'上海'},{name:'蚌埠南',value:29990}],
    // [{name:'上海南'},{name:'杭州东',value:599990}],
    // [{name:'上海南'},{name:'上海',value:40}],
    // [{name:'上海南'},{name:'上海虹桥',value:30000}],
    // [{name:'上海虹桥'},{name:'徐州东',value:20000}],
    // [{name:'上海虹桥'},{name:'南京',value:20000}],
    // [{name:'上海虹桥'},{name:'上海',value:200000}],
    // [{name:'上海虹桥'},{name:'上海南',value:20}],
    [{name:'杭州'},{name:'上海',value:94400}],
    [{name:'杭州'},{name:'南京',value:50900}],
    [{name:'杭州'},{name:'徐州',value:60000}],
    [{name:'杭州'},{name:'合肥',value:70000}],
    [{name:'杭州'},{name:'蚌埠',value:50000}],
    [{name:'杭州'},{name:'杭州南',value:40000}],
    [{name:'杭州'},{name:'上海虹桥',value:36000}],
    [{name:'杭州'},{name:'合肥南',value:20800}],
    [{name:'合肥西'},{name:'蚌埠',value:50000}],
    [{name:'合肥西'},{name:'杭州南',value:40090}],
    [{name:'合肥西'},{name:'上海虹桥',value:30090}],
    [{name:'合肥西'},{name:'南京',value:20080}],
    [{name:'徐州'},{name:'蚌埠',value:50080}],
    [{name:'徐州'},{name:'杭州南',value:41900}],
    [{name:'徐州'},{name:'上海虹桥',value:34900}],
    [{name:'徐州'},{name:'南京',value:20400}],
    [{name:'徐州'},{name:'合肥',value:20600}]
];

const planePath = 'path://m54.999994,123.625031l48.500004,-26.000023l48.500004,26.000023l-97.000007,0z'
const convertData = function (data) {
    let res = [];
    for (let i = 0; i < data.length; i++) {
        let dataItem = data[i];
        let fromCoord = geoCoordMap[dataItem[0].name];
        let toCoord = geoCoordMap[dataItem[1].name];
        if (fromCoord && toCoord) {
            res.push({
                fromName: dataItem[0].name,
                toName: dataItem[1].name,
                coords: [toCoord, fromCoord],
                value: dataItem[1].value,
            });
        }
    }
    return res;
};
let color = ['#ffb400'];
let series = [];
[['上海', SHData]].forEach(function (item, i) {
    // console.log(item,item[1]);
    series.push({
        type: 'lines',
        zlevel: 1,
        effect: {
            show: true,
            period: 6,
            trailLength: 0.7,
            color: '#fff',
            symbolSize: 2
        },
        lineStyle: {
            normal: {
                color: color[0],
                width: 0,
                curveness: 0.2
            }
        },
        data: convertData(item[1])
    },
    {
        type: 'lines',
        zlevel: 2,
        effect: {
            show: true,
            period: 6,
            trailLength: 0,
            symbol: 'arrow',
            symbolSize: 8
        },
        lineStyle: {
            normal: {
                color: color[0],
                width: 1,
                opacity: 0.4,
                curveness: 0.2
            }
        },
        data: convertData(item[1])
    },
    {
        name: item[0],
        type: 'effectScatter',
        coordinateSystem: 'geo',
        zlevel: 2,
        rippleEffect: {
            brushType: 'stroke'
        },
        label: {
            normal: {
                show: true,
                position: 'right',
                formatter: '{b}'
            }
        },
        symbolSize: function (val) {
            let _val = val[2]*12;
            if(_val < 10000){
                return 4;
            }else if (_val < 100000){
                return 6;
            }else if (_val < 1000000){
                return 10;
            }else if(_val < 10000000){
                return 13
            }else {
                return 16
            }
        },
        itemStyle: {
            itemStyle: {
                normal: {
                    color : (param) => {
                      let _val = param.value[2];
                      if(_val < 30000){
                        return '#49d9fe';
                      }else if (_val < 70000){
                        return '#638c0b';
                      }else if (_val < 80000){
                        return '#f39800';
                      }else if(_val < 90000){
                        return '#ff8200'
                      }else {
                        return '#af3100'
                      }
                    //   if(_val < 10000){
                    //     return '#49d9fe';
                    //   }else if (_val < 100000){
                    //     return '#638c0b';
                    //   }else if (_val < 1000000){
                    //     return '#f39800';
                    //   }else if(_val < 2000000){
                    //     return '#ff8200'
                    //   }else {
                    //     return '#af3100'
                    //   }
                    }
                }
            }
        },
        data: item[1].map(function (dataItem) {
            return {
                name: dataItem[1].name,
                value: geoCoordMap[dataItem[1].name].concat([dataItem[1].value])
            };
        })
    }
  );
});


const perCount = {
    '上海': {
        day: 1253890,
        all: 18690908
    },
    '上海南': {
        day: 1110890,
        all: 19690908
    },
    '上海虹桥': {
        day: 1160890,
        all: 15192122
    },
    '杭州': {
        day: 1025890,
        all: 13787602
    },
    '杭州东': {
        day: 1069930,
        all: 15690908
    },
    '杭州南': {
        day: 1163728,
        all: 16492022
    },
    '南京': {
        day: 1238191,
        all: 18727933
    },
    '南京南': {
        day: 1287289,
        all: 18372828
    },
    '徐州': {
        day: 1028382,
        all: 12839278
    },
    '徐州东': {
        day: 1037398,
        all: 16253616
    },
    '合肥': {
        day: 1190920,
        all: 14832729
    },
    '合肥南': {
        day: 1182736,
        all: 18023832
    },
    '合肥西': {
        day: 1028381,
        all: 12828918
    },
    '蚌埠': {
        day: 986890,
        all: 11093282
    },
    '蚌埠南': {
        day: 870890,
        all: 12980908
    },
}

export { series, perCount }
