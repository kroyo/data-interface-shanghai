// passengerPage 模拟数据


export const mockData = {
    radios: {
        leftTopData: 'year',
        leftCenterData: 'year',
        leftBottomData: 'year',
        centerTopData: 'day',
        centerLeftData: 'day',
        centerRightData: 'day'
    },
    selectes: {
      leftTop: [
        {
          value: '选项1',
          label: '合肥'
        }, {
          value: '选项2',
          label: '上海'
        }, {
          value: '南京',
          label: '南京'
        }, {
          value: '选项3',
          label: '杭州'
        }, {
          value: '选项4',
          label: '徐州'
        }, {
          value: '选项5',
          label: '蚌埠'
        }
      ],
      leftCenter: [
        {
          value: '选项1',
          label: '合肥'
        }, {
          value: '选项2',
          label: '上海'
        },{
          value: '南京',
          label: '南京'
        },  {
          value: '选项3',
          label: '杭州'
        }, {
          value: '选项4',
          label: '徐州'
        }, {
          value: '选项5',
          label: '蚌埠'
        }
      ],
      leftBottom: [
        {
          value: '选项1',
          label: '合肥'
        }, {
          value: '南京',
          label: '南京'
        }, {
          value: '选项2',
          label: '上海'
        }, {
          value: '选项3',
          label: '杭州'
        }, {
          value: '选项4',
          label: '徐州'
        }, {
          value: '选项5',
          label: '蚌埠'
        }
      ],
      centerTop: [
        {
          value: '合肥',
          label: '合肥'
        }, {
          value: '上海',
          label: '上海'
        },{
          value: '南京',
          label: '南京'
        }, {
          value: '杭州',
          label: '杭州'
        }, {
          value: '徐州',
          label: '徐州'
        }, {
          value: '蚌埠',
          label: '蚌埠'
        }
      ]
    },
    selected: {
      leftTop: '',
      leftCenter: '',
      leftBottom: '',
      centerTop: '',
    },
    waringData: {
      day : {
        all: {
          xName : '(时间/小时)',
          xData:['1', '2', '3', '4', '5', '6', '7','8','9','10','11','12'],
          line1:[20, 10, 20, 15, 50, 30, 70, 60, 80,90,120,100],
          line2:[50, 5, 26, 35, 50, 30, 40, 50, 40,70,10,60],
        }
      },
      week : {
        all: {
          xName : '(时间/天)',  
          xData:['1', '2', '3', '4', '5', '6', '7'],
          line1:[20, 10, 20, 15, 50, 30, 70],
          line2:[50, 5, 26, 35, 50, 30, 40, 50, 40],
        }
      },
      month : {
        all: {
          xName : '(时间/天)',  
          xData:['1', '2', '3', '4', '5', '6', '7','8','9','10','11','12'],
          line1:[20, 10, 20, 15, 50, 30, 70, 60, 80,90,120,100],
          line2:[50, 5, 26, 35, 50, 30, 40, 50, 40,70,10,60],
        }
      }
    },
    waringCountData: {
      day : {
        all: {
          ldata: [20,30,45],
          rdata: [20,30,45]
        }
      },
      week : {
        all: {
          ldata: [20,30,45],
          rdata: [20,30,45]
        }
      },
      month : {
        all: {
          ldata: [20,30,45],
          rdata: [20,30,45]
        }
      }
    }
}


