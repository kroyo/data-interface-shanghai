//字符串换行（用于Echart)
export function newline(params,length){
  var newParamsName = "";// 最终拼接成的字符串
  var paramsNameNumber = params.length;// 实际标签的个数
  var provideNumber = length;// 每行能显示的字的个数
  var rowNumber = Math.ceil(paramsNameNumber / provideNumber);// 换行的话，需要显示几行，向上取整
  /**
   * 判断标签的个数是否大于规定的个数， 如果大于，则进行换行处理 如果不大于，即等于或小于，就返回原标签
   */
  // 条件等同于rowNumber>1
  if (paramsNameNumber > provideNumber) {
      /** 循环每一行,p表示行 */
      for (var p = 0; p < rowNumber; p++) {
          var tempStr = "";// 表示每一次截取的字符串
          var start = p * provideNumber;// 开始截取的位置
          var end = start + provideNumber;// 结束截取的位置
          // 此处特殊处理最后一行的索引值
          if (p == rowNumber - 1) {
              // 最后一次不换行
              tempStr = params.substring(start, paramsNameNumber);
          } else {
              // 每一次拼接字符串并换行
              tempStr = params.substring(start, end) + "\n";
          }
          newParamsName += tempStr;// 最终拼成的字符串
      }

  } else {
      // 将旧标签的值赋给新标签
      newParamsName = params;
  }
  //将最终的字符串返回
  return newParamsName;
}
//数字每三位增加逗号
export function formatNum(n){
   var b=parseInt(n).toString();
   var len=b.length;
   if(len<=3){return b;}
   var r=len%3;
   return r>0?b.slice(0,r)+","+b.slice(r,len).match(/\d{3}/g).join(","):b.slice(r,len).match(/\d{3}/g).join(",");
 }
//数组切块
export const chunk = (arr, size) =>
  Array.from({length: Math.ceil(arr.length / size)}, (v, i) => arr.slice(i * size, i * size + size));



  // 数据赛选  x轴为时间
export function dataHandle(data){
    var hours=['00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23']
    var weekDays=getDateArr(7,'MM-dd');
    var monthDdays=getDateArr(30,'MM-dd');
    var ret={day_charts:{all:{}},week_charts:{all:{}},month_charts:{all:{}}}
    for(var k in data){
        var d = data[k];
        for(var i=0;i<d.length;i++){
            if(!ret[k][d[i].k1]){
                ret[k][d[i].k1]=[];
            }
            ret[k][d[i].k1].push(d[i]);
            if(!ret[k]['all'][d[i].k2]){
                ret[k]['all'][d[i].k2]={v1:0,v2:0,k2:d[i].k2}
            }
            ret[k]['all'][d[i].k2].v1+=(d[i].v1-0);
            ret[k]['all'][d[i].k2].v2+=(d[i].v2-0);
        }
    }
    for(var k in ret){
        var tmp = [];
        var all = ret[k].all;
        for(var i in all){
          tmp.push(all[i])
        }
        ret[k].all=tmp;
        if(k=='day_charts'){
            dataFill(ret[k],hours);
        }
        if(k=='week_charts'){
            dataFill(ret[k],weekDays);
        }
        if(k=='month_charts'){
           dataFill(ret[k],monthDdays);
        }
    }
    return ret;
};

// 不以时间作为坐标轴
export function dataHandle2(data){
    var ret={day_charts:{all:{}},week_charts:{all:{}},month_charts:{all:{}}}
    for(var k in data){
        var d = data[k];
        for(var i=0;i<d.length;i++){
            if(!ret[k][d[i].k1]){
                ret[k][d[i].k1]=[];
            }
            ret[k][d[i].k1].push(d[i]);
            if(!ret[k]['all'][d[i].k2]){
                ret[k]['all'][d[i].k2]={v1:0,v2:0,v3:0,k2:d[i].k2}
            }
            ret[k]['all'][d[i].k2].v1+=(d[i].v1-0);
            ret[k]['all'][d[i].k2].v2+=(d[i].v2-0);
            ret[k]['all'][d[i].k2].v3+=(d[i].v3-0);
        }
    }
    for(var k in ret){
        var tmp = [];
        var all = ret[k].all;
        for(var i in all){
          tmp.push(all[i])
        }
        ret[k].all=tmp;
    }
    if(Object.keys(ret['day_charts']['all']).length == 0){
        ret['day_charts']['all'].push({v1: 0, v2: 0, v3: 0, k2: "在逃人员"});
        ret['day_charts']['all'].push({v1: 0, v2: 0, v3: 0, k2: "重点人员"});
    }
    return ret;
}

Date.prototype.Format = function (fmt) { //author: meizz 
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日 
        "h+": this.getHours(), //小时 
        "m+": this.getMinutes(), //分 
        "s+": this.getSeconds(), //秒 
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
        "S": this.getMilliseconds() //毫秒 
    };
    if(!fmt){
        fmt='yyyy-MM-dd hh:mm:ss'
    }
    if (/(y+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    }
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) 
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}

  
function dataFill(sourceData,fillKeys){
    // if(sourceData.all[0] && sourceData.all[0].k2){
    //     if(sourceData.all[0].k2 == '在逃人员' || sourceData.all[0].k2 == '重点人员'){
    //         return;
    //     }
    // }
    for(var k1 in sourceData){
      var tmp1=sourceData[k1];
      var hs={}
      for(var j=0;j<tmp1.length;j++){
        if(tmp1[j].k2){
          hs[tmp1[j].k2]=j+1;
        }
      }
      var tmp2=[];
      for(var c=0;c<fillKeys.length;c++){
        if(!hs[fillKeys[c]]){
          tmp2.push({v1:0,v2:0,k2:fillKeys[c]})
        }else{
          tmp2.push(tmp1[hs[fillKeys[c]]-1]);
        }
      }
      sourceData[k1]=tmp2;
    }
}
  
  /** 
    * 获取日期数组
    * dayLen 日期长度
    */
function getDateArr(dayLen,format){
    var arr = [];
    var d = new Date();
    for(var i = 0;i<dayLen;i++){
        arr.unshift(new Date(d.getTime()-(24*60*60*1000)*i).Format(format));
    }
    return arr;
}



// 时间戳转日期

export function formatDateTime(inputTime) {  
    var date = new Date(inputTime);
    var y = date.getFullYear();  
    var m = date.getMonth() + 1;  
    m = m < 10 ? ('0' + m) : m;  
    var d = date.getDate();  
    d = d < 10 ? ('0' + d) : d;  
    return y + '-' + m + '-' + d;  
};